import type { Employee } from '@/types/Employee'
import http from './http'

function addEmployee(employee: any) {
  const emp = JSON.stringify(employee)
  return http.post('/employee', employee)
}

function updateEmployee(employeeId: number, employee: any) {
  return http.patch(`/employee/${employeeId}`, employee)
}

function delEmployee(employee: any) {
  return http.delete(`/employee/${employee.employeeId}`)
}

function getEmployee(id: number) {
  return http.get(`/employee/${id}`)
}

function getEmployees() {
  return http.get('/employee')
}

export default { addEmployee, updateEmployee, delEmployee, getEmployee, getEmployees }
