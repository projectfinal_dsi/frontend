type BranchStock = {
    branchStockId: number
    branchId: number
    materialId: number
    qty: number
}

export { type BranchStock }