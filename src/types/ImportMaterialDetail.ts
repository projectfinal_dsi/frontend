type ImportMaterialDetail = {
  importMaterialDetailId: number
  importMaterialId: number
  materialId: number
  qty: number
  unitPrice: number
  total: number
  branchStockId: number
}

export { type ImportMaterialDetail }
