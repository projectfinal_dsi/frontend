import type { ImportMaterialDetail } from './ImportMaterialDetail'
type ImportMaterial = {
  importMaterialId: number
  date: string
  total: number
  totalList: number
  employeeId: number
  branchId: number
  toggleStatus: boolean
  details: ImportMaterialDetail[]
}

export { type ImportMaterial }
