type Product = {
  productId?: number
  name: string
  price: number
  category: { name: 'Drink' | 'Food' | 'Dessert' }
  subCategory?: number //'เย็น' | 'ร้อน' | 'ปั่น'
  sweetLevel?: number //'0%' | '25%' | '50%' | '75%' | '100% '
  qty?: number //จำนวนคงเหลือ
}

export { type Product }
