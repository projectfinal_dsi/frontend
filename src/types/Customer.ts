type Customer = {
  customerId: number
  name: string
  phone: string
  birthDate: string
  point: number
}

export { type Customer }
