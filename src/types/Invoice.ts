import type { InvoiceDetail } from "./InvoiceDetail"

type Invoice = {
  invoiceId: number
  type: ('waterElectric' | 'material' | 'ค่าใช้จ่ายต่างๆ')
  branchId: number
  employeeId: number
  total: number
  paymentState: 'Paid' | 'Not Paid'
  datePay: string
  toggleStatus: boolean
  details: InvoiceDetail[]
}

export { type Invoice }
