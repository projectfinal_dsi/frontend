type ReceiptDetail = {
  receiptDetailId: number
  receiptId?: number
  qty: number
  price: number
  total: number
  productId: number
  productName: string
  productCategory?: 'Drink' | 'Food' | 'Dessert'
  productSubCategory?: number //'เย็น' | 'ร้อน' | 'ปั่น'
  productSweetLevel?: number //'0%' | '25%' | '50%' | '75%' | '100% '
}

export { type ReceiptDetail }
