type Salary = {
  salaryId: number
  worktimeId: number
  employeeId: number
  salaryCreate: string
  salaryPay?: string
  salaryTotal: number
  salaryStatus: 'Paid' | 'Not Paid'
}

export { type Salary }
