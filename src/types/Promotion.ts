type Promotion = {
  promotionId: number
  name: string
  startDate: string
  endDate: string
  status: 'active' | 'expired'
  discountP?: number
  discountB?: number
  usePoint?: number
  limitMenu?: number
}

export { type Promotion }
