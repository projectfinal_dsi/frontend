import type { Branch } from './Branch'
import type { Customer } from './Customer'
import type { Employee } from './Employee'
import type { Promotion } from './Promotion'

type Receipt = {
  receiptId: number
  branch: Branch
  employee: Employee
  customer: Customer
  promotion: Promotion
  queue?: number
  date: string
  totalBefore: number
  discount: number
  total: number
  receiveAmount?: number
  change?: number
  payment: 'Cash' | 'Promptpay'
  getPoint?: number
  customerPointBefore?: number
}

export { type Receipt }
