import type { Employee } from './Employee'

type CheckInOut = {
  checkInOutId?: number
  checkIn: string
  checkOut?: string
  timeWork?: number
  status?: number
  employee?: Employee
}

export { type CheckInOut }
