type CheckStockDetail = {
  checkStockDetailId: number
  materialId: number
  checkLast: number
  checkRemain: number
  used: number
  checkStockId: number
  branchStockId: number
}

export { type CheckStockDetail }
