import type { CheckStockDetail } from "./CheckStockDetail"
type CheckStock = {
  checkStockId: number
  date: string
  employeeId: number
  branchId: number
  details: CheckStockDetail[]
}

export { type CheckStock }
