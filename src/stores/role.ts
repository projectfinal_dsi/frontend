import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Role } from '@/types/Role'
import roleService from '@/services/role'
import type { Employee } from '@/types/Employee'

export const useRoleStore = defineStore('role', () => {
  const roles = ref<Role[]>([])
  const rolesName = ref<string[]>([])
  const getRoles = async () => {
    roles.value = []
    rolesName.value = []
    const resRoles = await roleService.getroles()
    for (const i of resRoles.data) {
      roles.value.push(i)
      rolesName.value.push(i.name)
    }
  }

  const roleStore = useRoleStore()
  const selectedRole = ref<string[]>([])

  return { roles, rolesName, getRoles }
})
