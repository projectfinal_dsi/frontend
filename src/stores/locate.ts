import { ref, computed, onMounted, watch } from 'vue'
import { useRoute } from 'vue-router'
import { defineStore } from 'pinia'

export const useLocateStore = defineStore('locate', () => {
  const appBarLocate = ref('')
  const route = useRoute()

  const locations = [
    { url: '/', name: 'Main Menu' },
    { url: '/pos', name: 'Point Of Sale' },
    { url: '/product', name: 'Product Management' },
    { url: '/employee', name: 'Employee Management' },
    { url: '/importmaterial', name: 'Import Material' },
    { url: '/customer', name: 'Customer' },
    { url: '/material', name: 'Material' },
    { url: '/salary', name: 'Salary Management' },
    { url: '/receipthistory', name: 'Receipt History' },
    { url: '/promotion', name: 'Promotion Management' },
    { url: '/branch', name: 'Branch' },
    { url: '/invoice', name: 'Utility Invoice' }
    // เพิ่ม URL และชื่อตามที่ต้องการ
  ]

  const locate = (place: string) => {
    appBarLocate.value = place
  }

  onMounted(() => {
    checkUrl()
  })

  // เช็ค URL ในหน้าปัจจุบัน
  const checkUrl = () => {
    const currentUrl = route.path
    const matchedLocation = locations.find((location) => location.url === currentUrl)
    if (matchedLocation) {
      appBarLocate.value = matchedLocation.name
    }
  }

  // ตรวจสอบ URL เมื่อมีการเปลี่ยนแปลง
  watch(route, () => {
    checkUrl()
  })

  return { appBarLocate, locate }
})
