import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Customer } from '@/types/Customer'
import { usePromotionStore } from './promotion'
import { useReceiptStore } from './receipt'

export const useCustomerStore = defineStore('customer', () => {
  const customers = ref<Customer[]>([
    {
      customerId: 1,
      name: 'แบม',
      phone: '01',
      birthDate: '16-03',
      point: 99
    },
    {
      customerId: 2,
      name: 'จิ๊ป',
      phone: '02',
      birthDate: '11-11',
      point: 19
    },
    {
      customerId: 3,
      name: 'อ๋า',
      phone: '03',
      birthDate: '11-11',
      point: 69
    }
  ])

  const search = (phone: string) => {
    const index = customers.value.findIndex((customer) => customer.phone === phone)
    takePoint.value = true
    currentCustomer.value = customers.value[index]
  }
  const currentCustomer = ref<Customer>({
    customerId: 0,
    name: '',
    phone: '',
    birthDate: '',
    point: 0
  })

  const clearCustomer = ref<Customer>({
    customerId: -1,
    name: '',
    phone: '',
    birthDate: '',
    point: 0
  })
  const initilCustomer: Customer = {
    customerId: -1,
    name: '',
    phone: '',
    birthDate: '',
    point: 0
    
}
  const editedCustomer = ref<Customer>(JSON.parse(JSON.stringify(initilCustomer)))
  const takePoint = ref(false)

  const telInput = ref('')

  const promotionStore = usePromotionStore()
  const receiptStore = useReceiptStore()

  function calculateDiscount() {
    const dateNow = new Date()
    const birthDateParts = currentCustomer.value.birthDate.split('-') // แยกวันเกิดออกเป็นส่วน
    const birthDay = parseInt(birthDateParts[0], 10) // วันที่
    const birthMonth = dateNow.getMonth() + 1 // เดือน (เพิ่ม 1 เนื่องจาก getMonth() เริ่มต้นที่ 0)

    // เช็คว่าวันเกิดเท่ากับวันปัจจุบันหรือไม่
    if (birthDay === dateNow.getDate() && birthMonth === dateNow.getMonth() + 1) {
      if (promotionStore.promotions[0].discountP)
        bdPercentage.value = promotionStore.promotions[0].discountP / 100
      receiptStore.bdPercentage = bdPercentage.value
    }
  }
  
  const customerDialog = ref(false)
  const bdPercentage = ref(0)
  //แนะนำว่าควรมี function ที่แปลงการแอดวันเดือนปีเกิดแบบตัวหนังสือให้เป็นตัวเลขเพื่อทำการเก็บ และมีfunctionแปลงตัวเลขไปเป็นตัวหนังสือเพื่อทำการแสดงผลบนตาราง
  //birthDate : MM-DD

  return {
    customers,
    search,
    currentCustomer,
    takePoint,
    clearCustomer,
    telInput,
    calculateDiscount,
    bdPercentage,
    customerDialog
  }
})
