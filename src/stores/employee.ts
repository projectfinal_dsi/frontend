import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Employee } from '@/types/Employee'
import { useLoadingStore } from './loading'
import employeeService from '@/services/employee'
import type { Role } from '@/types/Role'
import roleService from '@/services/role'
import { useRoleStore } from './role'

export const useEmployeeStore = defineStore('employee', () => {
  const employees = ref<Employee[]>([])
  const initaialEmployee = ref<Employee>({
    employeeId: 0,
    name: '',
    email: '',
    password: '',
    roles: [],
    gender: 'male',
    salaryType: 'Full-Time',
    phone: '',
    wagePerHour: 0,
    branch: []
  })

  const editedEmployee = ref<Employee>(JSON.parse(JSON.stringify(initaialEmployee)))

  async function getEmployee(id: number) {
    const res = await employeeService.getEmployee(id)
    editedEmployee.value = res.data
  }

  async function getEmployees() {
    const res = await employeeService.getEmployees()
    employees.value = res.data
  }

  async function saveEmployee() {
    const employeeData = {
      name: editedEmployee.value.name,
      email: editedEmployee.value.email,
      password: editedEmployee.value.password,
      roles: editedEmployee.value.roles.map((role) => ({ roleId: role.roleId })),
      gender: editedEmployee.value.gender,
      salaryType: editedEmployee.value.salaryType,
      salary: editedEmployee.value.salary,
      phone: editedEmployee.value.phone,
      wagePerHour: editedEmployee.value.wagePerHour,
      branch: editedEmployee.value.branch.map((branch) => ({
        branchId: branch.branchId
      }))
    }

    const empId = ref<number>(0)
    empId.value = editedEmployee.value.employeeId || 0
    if (editedEmployee.value.employeeId === 0) {
      const res = await employeeService.addEmployee(employeeData)
    } else {
      console.log(empId.value)
      console.log(JSON.stringify(employeeData))
      const res = await employeeService.updateEmployee(empId.value, employeeData)
    }

    await getEmployees()
  }

  async function deleteEmployee() {
    const employee = editedEmployee.value
    const res = await employeeService.delEmployee(employee)
  }

  function clearForm() {
    editedEmployee.value = JSON.parse(JSON.stringify(initaialEmployee))
  }
  const employeeDialog = ref(false)
  const selectedRoles = ref<string[]>([])
  return {
    getEmployee,
    getEmployees,
    saveEmployee,
    deleteEmployee,
    clearForm,
    employees,
    initaialEmployee,
    editedEmployee,
    employeeDialog,
    selectedRoles
  }
})
