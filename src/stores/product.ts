import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const Products = ref<Product[]>([])

  async function getProducts() {
    Products.value = []
    productsDrink.value = []
    productsDessert.value = []
    productsFood.value = []
    const res = await productService.getProducts()
    for (const jsonData of res.data) {
      const itemProduct: Product = {
        productId: jsonData.productId,
        name: jsonData.name,
        price: jsonData.price,
        category: { name: jsonData.category.name },
        qty: jsonData.qty
      }
      Products.value.push(itemProduct)
      if (itemProduct.category.name === 'Drink') {
        productsDrink.value.push(itemProduct)
      }
      if (itemProduct.category.name === 'Dessert') {
        productsDessert.value.push(itemProduct)
      }
      if (itemProduct.category.name === 'Food') {
        productsFood.value.push(itemProduct)
      }
    }
    // Products.value = res.data
    // return Products
  }

  const initialProduct: Product = {
    name: '',
    price: 0.0,
    category: { name: 'Drink' },
    qty: 0
  }
  const editedProduct = ref<Product>(JSON.parse(JSON.stringify(initialProduct)))

  //Data function
  async function getProduct(id: number) {
    loadingStore.doLoad()
    const res = await productService.getProduct(id)
    editedProduct.value = res.data
    loadingStore.finish()
  }

  // async function getProducts() {
  //   loadingStore.doLoad()
  //   const res = await productService.getProducts()
  //   editedProduct.value = res.data
  //   loadingStore.finish()
  // }

  async function saveProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    if (!product.productId) {
      //Add new
      const res = await productService.addProduct(product)
    } else {
      //update
      const res = await productService.updateProduct(product)
    }
    await getProducts()
    loadingStore.finish()
  }

  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)
    await getProducts()
    loadingStore.finish()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }

  const productsDrink = ref<Product[]>([])
  const productsDessert = ref<Product[]>([])
  const productsFood = ref<Product[]>([])

  // const productDialog = ref(false)

  return {
    getProducts,
    saveProduct,
    deleteProduct,
    editedProduct,
    getProduct,
    clearForm,
    Products,
    productsDessert,
    productsDrink,
    productsFood
  }
})
