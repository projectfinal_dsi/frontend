import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Material } from '@/types/Material'
import { useLoadingStore } from './loading'
import materialService from '@/services/material'
import material from '@/services/material'

export const useMaterialStore = defineStore('material', () => {
  const loadingStore = useLoadingStore()
  const Materials = ref<Material[]>([])
  const initialMaterial: Material = {
    materialId: -1,
    name: '',
    price: 0.0,
    minimum: 0,
    unit: ''
  }
  const editedMaterial = ref<Material>(JSON.parse(JSON.stringify(initialMaterial)))

  async function getMaterial(id: number) {
    const res = await materialService.getMaterial(id)
    editedMaterial.value = res.data
  }
  async function getMaterials() {
    const res = await materialService.getMaterials()
    Materials.value = res.data
  }

  async function saveMaterial() {
    loadingStore.doLoad()
    const material = editedMaterial.value
    if (!material.materialId) {
      //Add new
      const res = await materialService.addMaterial(material)
    } else {
      //update
      const res = await materialService.updateMaterial(material)
    }
    await getMaterials()
    loadingStore.finish()
  }

  async function deleteMaterial() {
    const Material = editedMaterial.value
    const res = await materialService.delMaterial(Material)

    await getMaterials()
  }

  function clearForm() {
    editedMaterial.value = JSON.parse(JSON.stringify(initialMaterial))
  }

  return {
    getMaterials,
    deleteMaterial,
    editedMaterial,
    getMaterial,
    clearForm,
    Materials,
    saveMaterial
  }
})
