import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'

export const usePromotionStore = defineStore('promotion', () => {
  const promotions = ref<Promotion[]>([
    {
      promotionId: 1,
      name: 'วันเกิดลด 5%',
      startDate: '2023-01-01',
      endDate: '2123-01-01',
      status: 'active',
      discountP: 5
    },
    {
      promotionId: 2,
      name: '10 แต้มฟรี 1 แก้ว',
      startDate: '2023-01-01',
      endDate: '2123-01-01',
      status: 'active',
      discountP: 100,
      usePoint: 10,
      limitMenu: 1
    },

    {
      promotionId: 3,
      name: '10 แต้มลด 5% ทุกรายการ',
      startDate: '2023-01-01',
      endDate: '2123-01-01',
      status: 'active',
      discountP: 5,
      usePoint: 10
    },
    {
      promotionId: 4,
      name: '20 แต้มลด 60 บาท',
      startDate: '2023-01-01',
      endDate: '2024-01-01',
      status: 'expired',
      discountB: 60,
      usePoint: 20
    }
  ])

  const promotionDialog = ref(false)

  return { promotions, promotionDialog }
})
