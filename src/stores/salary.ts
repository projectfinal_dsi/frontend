import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Salary } from '@/types/Salary'

export const useSalaryStore = defineStore('salary', () => {
  const salaries = ref<Salary[]>([
    {
      salaryId: 1,
      worktimeId: 1,
      employeeId: 1,
      salaryCreate: '2023-12-01', //รอบของเดือนธันวา ปี 66
      salaryPay: '2024-01-02',
      salaryTotal: 23000,
      salaryStatus: 'Paid'
    },
    {
      salaryId: 2,
      worktimeId: 2,
      employeeId: 2,
      salaryCreate: '2023-12-01',
      salaryPay: '2024-01-02',
      salaryTotal: 22500,
      salaryStatus: 'Paid'
    },
    {
      salaryId: 3,
      worktimeId: 3,
      employeeId: 3,
      salaryCreate: '2023-12-01',
      salaryTotal: 3360,
      salaryStatus: 'Not Paid'
    },
    {
      salaryId: 4,
      worktimeId: 4,
      employeeId: 4,
      salaryCreate: '2023-12-01',
      salaryTotal: 4000,
      salaryStatus: 'Not Paid'
    },
    {
      salaryId: 5,
      worktimeId: 5,
      employeeId: 5,
      salaryCreate: '2023-12-01',
      salaryPay: '2024-01-02',
      salaryTotal: 12000,
      salaryStatus: 'Paid'
    }
  ])
  const createSalaryDialog = ref(false)
  const generatePaymentSlip = ref(false)

  function deleteSalary(id: number) {
    const index = salaries.value.findIndex(salary => salary.salaryId === id)
    if (index !== -1) {
      salaries.value.splice(index, 1)
    }
  }

  return { salaries, createSalaryDialog, deleteSalary, generatePaymentSlip}
})
