import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { ImportMaterial } from '@/types/ImportMaterial'
import type { ImportMaterialDetail } from '@/types/ImportMaterialDetail'

export const useImportMaterialStore = defineStore('importMaterial', () => {
  const importMaterials = ref<ImportMaterial[]>([
    {
      importMaterialId: 1,
      date: '2024-01-02',
      total: 3550,
      totalList: 3,
      employeeId: 1,
      branchId: 1,
      toggleStatus: false,
      details: []
    },
    {
      importMaterialId: 2,
      date: '2024-02-03',
      total: 3600,
      totalList: 2,
      employeeId: 3,
      branchId: 2,
      toggleStatus: false,
      details: []
    }
  ])
  const importMaterialDetails = ref<ImportMaterialDetail[]>([
    {
      importMaterialDetailId: 1,
      importMaterialId: 1,
      materialId: 1,
      qty: 3,
      unitPrice: 400,
      total: 1200,
      branchStockId: 1
    },
    {
      importMaterialDetailId: 2,
      importMaterialId: 1,
      materialId: 2,
      qty: 10,
      unitPrice: 150,
      total: 1500,
      branchStockId: 2
    },
    {
      importMaterialDetailId: 3,
      importMaterialId: 1,
      materialId: 3,
      qty: 5,
      unitPrice: 170,
      total: 850,
      branchStockId: 3
    },
    {
      importMaterialDetailId: 4,
      importMaterialId: 2,
      materialId: 4,
      qty: 20,
      unitPrice: 90,
      total: 1800,
      branchStockId: 4
    },
    {
      importMaterialDetailId: 5,
      importMaterialId: 2,
      materialId: 5,
      qty: 5,
      unitPrice: 360,
      total: 1800,
      branchStockId: 5
    }
  ])

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
  }

  const editingimportDetails = ref<ImportMaterialDetail[]>([])

  //push details to importmaterial
  importMaterials.value.forEach((importMaterial) => {
    importMaterial.details = importMaterialDetails.value.filter(
      (detail) => detail.importMaterialId === importMaterial.importMaterialId
    )
  })

  const lastID = 2
  const detailID = 0
  const importMaterialDialog = ref(false)
  const importMaterialDetailsDialog = ref(false)
  return {
    importMaterials,
    importMaterialDetails,
    importMaterialDialog,
    importMaterialDetailsDialog,
    editingimportDetails,
    detailID,
    lastID
  }
})
