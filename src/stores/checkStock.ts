import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { CheckStock } from '@/types/CheckStock'
import type { CheckStockDetail } from '@/types/CheckStockDetail'
import { useMaterialStore } from './material'


export const useCheckStockStore = defineStore('checkStock', () => {
  const materialStore = useMaterialStore()
  const checkStocks = ref<CheckStock[]>([
    {
      checkStockId: 1,
      date: '2024-01-01',
      employeeId: 5,
      branchId: 1,
      details: []
    },
    {
      checkStockId: 2,
      date: '2024-02-02',
      employeeId: 3,
      branchId: 2,
      details: []
    }
  ])

  const checkStockDetails = ref<CheckStockDetail[]>([
    {
      checkStockDetailId: 1,
      materialId: 1,
      checkLast: 50,
      checkRemain: 15,
      used: 35,
      checkStockId: 1,
      branchStockId: 1
    },
    {
      checkStockDetailId: 2,
      materialId: 2,
      checkLast: 50,
      checkRemain: 25,
      used: 25,
      checkStockId: 1,
      branchStockId: 2
    },
    {
      checkStockDetailId: 3,
      materialId: 3,
      checkLast: 50,
      checkRemain: 45,
      used: 5,
      checkStockId: 1,
      branchStockId: 3
    },
    {
      checkStockDetailId: 4,
      materialId: 4,
      checkLast: 50,
      checkRemain: 16,
      used: 34,
      checkStockId: 1,
      branchStockId: 4
    },
    {
      checkStockDetailId: 5,
      materialId: 5,
      checkLast: 50,
      checkRemain: 34,
      used: 16,
      checkStockId: 1,
      branchStockId: 5
    },
    {
      checkStockDetailId: 6,
      materialId: 1,
      checkLast: 80,
      checkRemain: 30,
      used: 50,
      checkStockId: 2,
      branchStockId: 1
    },
    {
      checkStockDetailId: 7,
      materialId: 2,
      checkLast: 80,
      checkRemain: 45,
      used: 35,
      checkStockId: 2,
      branchStockId: 2
    },
    {
      checkStockDetailId: 8,
      materialId: 3,
      checkLast: 80,
      checkRemain: 50,
      used: 30,
      checkStockId: 2,
      branchStockId: 3
    },
    {
      checkStockDetailId: 9,
      materialId: 4,
      checkLast: 80,
      checkRemain: 60,
      used: 20,
      checkStockId: 2,
      branchStockId: 4
    },
    {
      checkStockDetailId: 10,
      materialId: 5,
      checkLast: 80,
      checkRemain: 60,
      used: 20,
      checkStockId: 2,
      branchStockId: 5
    }
  ])

  const checkStockDialog = ref(false)

  function newCheckStock() {
    checkStockDialog.value = true
  }

  const createCheckStock = (dmy: any) => {
    const newCheckStockReceipt = ref<CheckStock>({
      checkStockId: 1,
      employeeId: 0,
      branchId: 0,
      date: '',
      details: []
    })
    newCheckStockReceipt.value.checkStockId = checkStocks.value[checkStocks.value.length - 1].checkStockId + 1
    newCheckStockReceipt.value.date = dmy
    checkStocks.value.push(newCheckStockReceipt.value)
  }

  const setDetail = (details: CheckStockDetail[]) => {
    checkStocks.value[checkStocks.value.length - 1].details = details
  }

  const delLast = ref(false)
  watch(delLast, (newValue) => {
    const rollBackQty = ref<number[]>([])
    for (const item of checkStocks.value[checkStocks.value.length - 1].details) {
      rollBackQty.value.push(item.checkLast)
    }
    //materialStore.updateQty(rollBackQty.value)
    delLast.value = false
  })

  const confirmDelete = () => {
    delLast.value = true
  }

  //details
  const checkStockDetailData = ref<CheckStockDetail[]>([])
  const newCheckStockDetails = ref<CheckStockDetail[]>([])

  const getDetailData = () => {
    const sendCheckDetail = ref<CheckStockDetail[]>([])
    sendCheckDetail.value = newCheckStockDetails.value
    newCheckStockDetails.value = []
    return sendCheckDetail
  }

  const createCheckStockDetail = (index: number, chkId: number, chkRemain: number) => {
    const latestId =
      checkStockDetailData.value.length > 0
        ? Math.max(...checkStockDetailData.value.map((detail) => detail.checkStockDetailId))
        : 0

    const newCheckStockDetail = ref<CheckStockDetail>({
      checkStockDetailId: 0,
      checkStockId: 0,
      materialId: 0,
      //material: '',
      checkLast: 0,
      checkRemain: 0,
      used: 0,
      branchStockId: 0
    })
    console.log('ดีเทล')
    newCheckStockDetail.value.checkStockDetailId = latestId + 1
    newCheckStockDetail.value.checkStockId = chkId
    newCheckStockDetail.value.materialId = materialStore.materials[index].materialId
    // newCheckStockDetail.value.material = materialStore.materials[index].Name
    // newCheckStockDetail.value.checkLast = materialStore.materials[index].qty
    newCheckStockDetail.value.checkRemain = chkRemain
    newCheckStockDetails.value.push(newCheckStockDetail.value)
    checkStockDetailData.value.push(newCheckStockDetail.value)
  }


  return { checkStocks, 
    checkStockDialog, 
    checkStockDetails, 
    newCheckStock, 
    setDetail, 
    confirmDelete,
    createCheckStock,
    getDetailData,
    createCheckStockDetail}
})
