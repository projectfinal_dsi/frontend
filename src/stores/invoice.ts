import { type InvoiceDetail } from "@/types/InvoiceDetail"
import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Invoice } from '@/types/Invoice'

export const useInvoiceStore = defineStore('invoice', () => {
  const invoices = ref<Invoice[]>([
    {
      invoiceId: 1,
      type: 'waterElectric',
      branchId: 1,
      employeeId: 1,
      total: 825,
      paymentState: 'Paid',
      datePay: '2024-01-01',
      toggleStatus: false,
      details: []
    },
    {
      invoiceId: 2,
      type: 'ค่าใช้จ่ายต่างๆ',
      branchId: 1,
      employeeId: 1,
      total: 320,
      paymentState: 'Paid',
      datePay: '2024-01-02',
      toggleStatus: false,
      details: []
    }
  ])

  const invoiceDetails = ref<InvoiceDetail[]>([
    {
      invoiceDetailId: 1,
      invoiceId: 1,
      item: 'water',
      qty: 9,
      price: 25,
      total: 225,
      unit: 'หน่วย'
    },
    {
      invoiceDetailId: 2,
      invoiceId: 1,
      item: 'eletric',
      qty: 100,
      price: 6,
      total: 600,
      unit: 'หน่วย'
    },
    {
      invoiceDetailId: 3,
      invoiceId: 2,
      item: 'หลอด',
      qty: 30,
      price: 2,
      total: 60,
      unit: 'ถุง(100 อัน)'
    },
    {
      invoiceDetailId: 4,
      invoiceId: 2,
      item: 'แก้ว',
      qty: 100,
      price: 4,
      total: 240,
      unit: 'ถุง(100 อัน)'
    },
    {
      invoiceDetailId: 5,
      invoiceId: 2,
      item: 'ซันไล',
      qty: 20,
      price: 1,
      total: 20,
      unit: 'ถุง'
    }
  ])

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
  }

  const editinginvoiceDetails = ref<InvoiceDetail[]>([])

  //push details to invoicematerial
  invoices.value.forEach((invoice: any) => {
    invoice.details = invoiceDetails.value.filter(
      (detail: any) => detail.invoiceId === invoice.id
    )
  })

  const lastID = 2
  const detailID = 0
  const invoiceDialog = ref(false)
  const invoiceDetailsDialog = ref(false)

  return { invoices, invoiceDetails, lastID, detailID, invoiceDialog, invoiceDetailsDialog, formatTime, editinginvoiceDetails }
})
