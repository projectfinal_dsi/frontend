import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { WorkTime } from '@/types/WorkTime'

export const useWorkTimeStore = defineStore('worktime', () => {
  const worktimes = ref<WorkTime[]>([
    {
      workTimeId: 1,
      employeeId: 1,
      totalTime: 10,
      overTime: 2
    },
    {
      workTimeId: 2,
      employeeId: 2,
      totalTime: 7
    },
    {
      workTimeId: 3,
      employeeId: 3,
      totalTime: 3
    },
    {
      workTimeId: 4,
      employeeId: 4,
      totalTime: 3
    },
    {
      workTimeId: 5,
      employeeId: 5,
      totalTime: 8
    }
  ])

  return { worktimes }
})
