import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Employee } from '@/types/Employee'
import { useEmployeeStore } from './employee'

export const useAuthStore = defineStore('auth', () => {
  const currentLogin = ref<Employee>({
    employeeId: 1,
    name: 'Peerada Wangyaichim',
    email: 'Peerada@gmail.com',
    password: 'password',
    roles: [],
    gender: 'female',
    salaryType: 'Full-Time',
    salary: 23000,
    phone: '081',
    wagePerHour: 143.75,
    branch: []
  })

  const EmployeeStore = useEmployeeStore()

  // const methods = {
  //   authenticateUser(): boolean {
  //     return currentLogin.value.employeeId > -1
  //   },
  //   grantAccess(routeRoles: Employee['roles']): boolean {
  //     return routeRoles.every((role) => currentLogin.value.roles.includes(role))
  //   },
  //   setCurrentUser(newUser: Employee): void {
  //     currentLogin.value = newUser
  //     const parsed = JSON.stringify(newUser)
  //     localStorage.setItem('user', parsed)
  //   },
  //   checkUser(email: string, password: string): boolean {
  //     const foundUser = EmployeeStore.$state.employees.find(
  //       (employee: { email: string; password: string }) =>
  //         employee.email === email && employee.password === password
  //     )

  //     if (foundUser) {
  //       this.setCurrentUser(foundUser)
  //       return true
  //     }
  //     return false
  //   },
  //   getCurrentUser(): Employee {
  //     return { ...currentLogin.value }
  //   },
  //   clearUser() {
  //     localStorage.removeItem('user')
  //   }
  // }

  return { currentLogin }
})
