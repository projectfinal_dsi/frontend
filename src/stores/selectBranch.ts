import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useSelectBranchStore = defineStore('selectbranch', () => {
  const dialogOpen = ref(false)

  const branches = [
    { id: 1, name: 'ทั้งหมด' },
    { id: 2, name: 'บางแสน' },
    { id: 3, name: 'อ่างศิลา' },
    { id: 4, name: 'ศรีราชา' }
  ]

  const selectBranch = (branch: { id: number; name: string }) => {
    dialogOpen.value = false
    selectedBranch.value = branch.name
  }

  const selectedBranch = ref('บางแสน')

  return { selectBranch, branches, dialogOpen, selectedBranch }
})
