import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { BranchStock } from '@/types/BranchStock'

export const useBranchStockStore = defineStore('branchStore', () => {
  const branchStocks = ref<BranchStock[]>([
    {
      branchStockId: 1,
      branchId: 1,
      materialId: 1,
      qty: 20
    },
    {
      branchStockId: 2,
      branchId: 1,
      materialId: 2,
      qty: 30
    },
    {
      branchStockId: 3,
      branchId: 1,
      materialId: 3,
      qty: 52
    },
    {
      branchStockId: 4,
      branchId: 2,
      materialId: 4,
      qty: 42
    },
    {
      branchStockId: 5,
      branchId: 2,
      materialId: 5,
      qty: 21
    }
  ])

  return { branchStocks }
})
