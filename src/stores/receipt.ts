import { ref, computed, watch, onMounted } from 'vue'
import { defineStore } from 'pinia'
import type { Receipt } from '@/types/Receipt'
import type { ReceiptDetail } from '@/types/ReceiptDetail'
import type { Product } from '@/types/Product'
// import type { ReceiptItem } from '@/typesOld/ReceiptItem'
import { useCustomerStore } from './customer'
import { useEmployeeStore } from './employee'
import { useAuthStore } from './auth'
import type { Promotion } from '@/types/Promotion'

export const useReceiptStore = defineStore('receipt', () => {
  const receipts = ref<Receipt[]>([
    {
      receiptId: 1,
      branchId: 1,
      employeeId: 5,
      customerId: 1,
      date: '02/01/2024 09:00:00',
      totalBefore: 200,
      discount: 0,
      total: 200,
      receiveAmount: 200,
      change: 0,
      payment: 'Promptpay',
      getPoint: 4,
      customerPointBefore: 99
    },
    {
      receiptId: 2,
      branchId: 2,
      employeeId: 5,
      date: '02/01/2024 09:30:00',
      totalBefore: 150,
      discount: 0,
      total: 150,
      receiveAmount: 200,
      change: 50,
      payment: 'Cash'
    }
  ])

  const receiptDetails = ref<ReceiptDetail[]>([
    {
      receiptDetailId: 1,
      receiptId: 1,
      qty: 4,
      price: 50,
      total: 200,
      productId: 1,
      productName: 'คาปูชิโน่',
      productCategory: 'Drink',
      productSubCategory: 1
    },
    {
      receiptDetailId: 2,
      receiptId: 2,
      qty: 3,
      price: 50,
      total: 150,
      productId: 1,
      productName: 'คาปูชิโน่',
      productCategory: 'Drink',
      productSubCategory: 1
    }
  ])

  const tempProduct = ref<Product>() //สำหรับเลือก option ใส่ product

  const openOptionProduct = ref(false) //สำหรับเปิดหน้าเลือก option

  const selectP = (
    qty: number,
    subCategory: number,
    sweetLevel: number,
    totalPriceOneQty: number
  ) => {
    const receiptItem1 = ref<ReceiptDetail>({
      receiptDetailId: 0,
      qty: qty,
      price: totalPriceOneQty,
      total: 0,
      productId: 0,
      productName: ''
    })
    if (receiptItem1.value && tempProduct.value) {
      receiptItem1.value.total = receiptItem1.value.price * receiptItem1.value.qty
      if (tempProduct.value?.productId) receiptItem1.value.productId = tempProduct.value?.productId
      receiptItem1.value.productName = tempProduct.value.name
      receiptItem1.value.productCategory = tempProduct.value.category.name
      receiptItem1.value.productSubCategory = subCategory
      if (
        tempProduct.value.category.name === 'Dessert' ||
        tempProduct.value.category.name === 'Food'
      ) {
        receiptItem1.value.productSubCategory = undefined
      }
      receiptItem1.value.productSweetLevel = sweetLevel
      if (
        tempProduct.value.category.name === 'Dessert' ||
        tempProduct.value.category.name === 'Food'
      ) {
        receiptItem1.value.productSweetLevel = undefined
      }
    }

    const matchedItem = receiptItems.value.find(
      (item) =>
        item.productId === receiptItem1.value.productId &&
        item.productSubCategory === receiptItem1.value.productSubCategory &&
        item.productSweetLevel === receiptItem1.value.productSweetLevel
    )
    if (matchedItem) {
      matchedItem.qty += qty
      receiptItems.value = receiptItems.value.filter((item) => (item.total = item.qty * item.price))
      calTotalBefore(receiptItems.value)
    } else {
      receiptItems.value.push(receiptItem1.value)
    }

    calTotalBefore(receiptItems.value)
  }

  const removeReceiptItem = (itemToRemove: ReceiptDetail) => {
    receiptItems.value = receiptItems.value.filter((item) => item !== itemToRemove)
    calTotalBefore(receiptItems.value)
  }
  const discount = ref<number>(0)
  const dec = (item: ReceiptDetail) => {
    item.qty -= 1
    receiptItems.value = receiptItems.value.filter((item) => (item.total = item.qty * item.price))
    calTotalBefore(receiptItems.value)
  }

  const inc = (item: ReceiptDetail) => {
    item.qty += 1
    receiptItems.value = receiptItems.value.filter((item) => (item.total = item.qty * item.price))
    calTotalBefore(receiptItems.value)
  }
  const receiptItems = ref<ReceiptDetail[]>([])
  const bdPercentage = ref(0)

  const calTotalBefore = (receiptItems: ReceiptDetail[]) => {
    let totalBefore = 0

    for (const item of receiptItems) {
      totalBefore += item.total
    }

    initialReceipt.value.totalBefore = totalBefore
    discount.value = bdPercentage.value * initialReceipt.value.totalBefore
    initialReceipt.value.discount = discount.value
    initialReceipt.value.total = initialReceipt.value.totalBefore - discount.value

    discount.value = (promotionPercentage.value / 100) * initialReceipt.value.total
    initialReceipt.value.discount += discount.value
    initialReceipt.value.total = initialReceipt.value.total - discount.value

    discount.value = promotionBath.value
    initialReceipt.value.discount += discount.value
    initialReceipt.value.total = initialReceipt.value.total - discount.value

    if (receiptItems && promotionNow.value?.limitMenu) {
      const qtyNow = ref(0)
      for (const item of receiptItems) {
        qtyNow.value += item.qty
        if (qtyNow.value > promotionNow.value?.limitMenu) {
          alert('โปรโมชั่นนี้ไม่รองรับ')
          clearReceipt()
        }
      }
    }
  }

  watch(bdPercentage, () => {
    calTotalBefore(receiptItems.value)
  })

  const clearReceiptDeatil = ref<ReceiptDetail[]>([])
  const initialReceipt = ref<Receipt>({
    receiptId: 0,
    branchId: 0,
    employeeId: 0,
    date: '',
    totalBefore: 0,
    discount: 0,
    total: 0,
    payment: 'Cash'
  })

  const receiveMoney = ref<number>(0)

  const tempPoint = ref<number>(0)

  const selectedPayment = ref()

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
  }

  const authStore = useAuthStore()
  const currentQueue = ref(1)
  watch(
    () => authStore.currentLogin.employeeId,
    (newVal) => {
      if (newVal === 0) {
        currentQueue.value = 1
      }
    }
  )

  const saveReceipt = () => {
    initialReceipt.value.receiptId = receipts.value.length + 1
    // initialReceipt.value.branchId

    initialReceipt.value.employee = authStore.currentLogin //ส่ง
    initialReceipt.value.customer = customerStore.currentCustomer //ส่ง

    initialReceipt.value.queue = currentQueue.value //ส่ง
    initialReceipt.value.date = formatTime(new Date()) //ส่ง
    initialReceipt.value.receiveAmount = receiveMoney.value //ส่ง
    if (initialReceipt.value.receiveAmount !== undefined)
      initialReceipt.value.change = initialReceipt.value.receiveAmount - initialReceipt.value.total //ส่ง
    initialReceipt.value.payment = selectedPayment.value //ส่ง
    if (promotionNow.value?.usePoint)
      initialReceipt.value.customerPointBefore =
        customerStore.currentCustomer.point - promotionNow.value?.usePoint //ส่ง
    initialReceipt.value.getPoint = getPoint.value //ส่ง

    for (const item of receiptItems.value) {
      item.receiptDetailId = receiptDetails.value.length + 1
      item.receiptId = initialReceipt.value.receiptId
      receiptDetails.value.push(item)
    }
    const cusIndex = customerStore.customers.findIndex((customer) => {
      if (initialReceipt.value.customerId) {
        return customer.customerId === initialReceipt.value.customerId
      }
      return false
    })
    if (cusIndex !== -1 && customerStore.customers[cusIndex]) {
      customerStore.customers[cusIndex].point += getPoint.value
    }
    //minus point
    if (cusIndex !== -1 && customerStore.customers[cusIndex]) {
      if (promotionNow.value?.usePoint)
        customerStore.customers[cusIndex].point -= promotionNow.value?.usePoint
    }
    descriptReceipt.value = initialReceipt.value
    descriptReceiptDetail.value = receiptItems.value
    receipts.value.push(initialReceipt.value)
    currentQueue.value += 1

    // promotionNow.value?.usePoint
    if (promotionNow.value) initialReceipt.value.promotion = promotionNow.value
    clearReceipt()
    console.log('DesNowReceipt')
    console.log(descriptReceipt.value)
    console.log('DesNowDetail')
    console.log(descriptReceiptDetail.value)
    console.log('NowFullReceit')
    console.log(receipts.value)
    console.log('NowFullDetail')
    console.log(receiptDetails.value)
  }

  const clearReceipt = () => {
    const cleanReceipt = ref<Receipt>({
      receiptId: 0,
      branchId: 0,
      employeeId: 0,
      date: '',
      totalBefore: 0,
      discount: 0,
      total: 0,
      payment: 'Cash'
    })
    initialReceipt.value = cleanReceipt.value
    receiptItems.value = []
    discount.value = 0
    customerStore.takePoint = false
    customerStore.currentCustomer = customerStore.clearCustomer
    tempPoint.value = 0
    receiveMoney.value = 0
    selectedPayment.value = 'Cash'
    customerStore.telInput = ''
    // receiptDialog.value = false
    calTotalBefore
    openOptionProduct.value = false
    bdPercentage.value = 0
    happybirthday.value = false
    promotionBath.value = 0
    promotionPercentage.value = 0
    promotionNow.value = undefined
  }
  const happybirthday = ref(false)
  const getPoint = ref(0)

  const receiptDialog = ref(false)

  const customerStore = useCustomerStore()

  const ppDialog = ref(false)

  const seeReceiptHistory = (r: Receipt) => {
    descriptReceipt.value = r
    const filteredReceipts = receiptDetails.value.filter((item) => item.receiptId === r.receiptId)
    descriptReceiptDetail.value = filteredReceipts ? filteredReceipts.flat() : []
    receiptDialog.value = true
  }
  const descriptReceipt = ref<Receipt>({
    receiptId: 0,
    branchId: 0,
    employeeId: 0,
    date: '',
    totalBefore: 0,
    discount: 0,
    total: 0,
    payment: 'Promptpay'
  })
  const descriptReceiptDetail = ref<ReceiptDetail[]>([])

  const promotionPercentage = ref(0)
  const promotionBath = ref(0)
  const promotionNow = ref<Promotion>()

  watch(promotionBath, () => {
    calTotalBefore(receiptItems.value)
  })
  watch(promotionPercentage, () => {
    calTotalBefore(receiptItems.value)
  })
  watch(
    () => initialReceipt.value.total,
    () => {
      if (initialReceipt.value.total < 0) alert('กรุณาใส่รายการเพิ่มเติม')
    }
  )
  return {
    receipts,
    receiptDetails,
    tempProduct,
    openOptionProduct,
    selectP,
    receiptItems,
    inc,
    dec,
    initialReceipt,
    selectedPayment,
    removeReceiptItem,
    receiveMoney,
    tempPoint,
    saveReceipt,
    clearReceipt,
    getPoint,
    receiptDialog,
    clearReceiptDeatil,
    ppDialog,
    seeReceiptHistory,
    descriptReceipt,
    descriptReceiptDetail,
    currentQueue,
    discount,
    bdPercentage,
    happybirthday,
    promotionBath,
    promotionPercentage,
    promotionNow
  }
})
