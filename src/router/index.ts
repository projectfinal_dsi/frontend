import { createRouter, createWebHistory } from 'vue-router'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'mainmenu',
      component: () => import('../views/MainMenuView.vue')
    },
    {
      path: '/pos',
      name: 'pos',
      component: () => import('../views/POS/posView.vue')
    },
    {
      path: '/product',
      name: 'product',
      component: () => import('../views/Product/ProductView.vue')
    },
    {
      path: '/employee',
      name: 'employee',
      component: () => import('../views/Employee/EmployeeView.vue')
    },
    {
      path: '/customer',
      name: 'customer',
      component: () => import('../views/Customer/CustomerView.vue')
    },
    {
      path: '/importmaterial',
      name: 'importmaterial',
      component: () => import('../views/ImportMaterial/ImportMaterialView.vue')
    },
    {
      path: '/material',
      name: 'material',
      component: () => import('../views/Material/MaterialView.vue')
    },
    {
      path: '/checkinout',
      name: 'checkinout',
      component: () => import('../views/CheckInOut/CheckInOut.vue')
    },
    {
      path: '/checkstock',
      name: 'checkstock',
      component: () => import('../views/CheckStock/CheckstockView.vue')
    },
    {
      path: '/salary',
      name: 'salary',
      component: () => import('../views/Salary/SalaryView.vue')
    },
    {
      path: '/receipthistory',
      name: 'receipthistory',
      component: () => import('@/views/POS/ReceiptHistoryView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginPage.vue')
    },
    {
      path: '/promotion',
      name: 'promotion',
      component: () => import('../views/Promotion/PromotionView.vue')
    },
    {
      path: '/branch',
      name: 'branch',
      component: () => import('../views/Branch/BranchView.vue')
    },
    {
      path: '/invoice',
      name: 'invoice',
      component: () => import('../views/UtilityInvoice/InvoiceView.vue')
    },
    {
      path: '/oneEmployeeManagement',
      name: 'oneEmployeeManagement',
      component: () => import('../views/OneEmployeeManagement.vue')
    }
  ]
})

export default router
